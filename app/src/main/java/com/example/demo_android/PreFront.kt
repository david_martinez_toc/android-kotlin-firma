package com.example.demo_android

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
//import androidx.appcompat.app.ActivityCompat
//import androidx.appcompat.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cl.toc.labs.autocapture.CaptureActivity
import io.fotoapparat.selector.back
import okhttp3.*
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class PreFront : AppCompatActivity()  {

   private var btnACfront: Button? = null
   private var btnACback: Button? = null

    private var flag: String? = null
    private var document_type: String? = null

    private var imgFlag: ImageView? = null
    private var txtDocType: TextView? = null
    private var txtEdit: TextView? = null

    private var txtInfoAC: TextView? = null
    private var txtachint: TextView? = null
    private var txtRepeatFront: TextView? = null
    private var txtRepeatBack: TextView? = null
    private var txt1st: TextView? = null

    private var btnBack: ImageView? = null
    private var session_id: String? = null
    private var txtPreFront: TextView? = null
    private var front_token: String? = null
    private var back_token: String? = null
    private var liveness_token: String? = null

    private var layout_acfront: View? = null
    private var layout_acinfo: View? = null
    private var boxHints: View? = null
    private var boxResult: View? = null

    private var btnClose: ImageView? = null
    private var btnACnext: Button? = null
    private var btnACnext2: Button? = null

    private var imgFront: ImageView? = null

    private var imgDocSide: ImageView? = null
    private var loadingFrame: FrameLayout? = null


    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_pre_front)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                1
            )
        }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
        }

        flag = intent.getStringExtra("doc_flag")
        document_type = intent.getStringExtra("doc_type")

        layout_acfront = findViewById(R.id.layout_acfront)
        layout_acinfo = findViewById(R.id.layout_acinfo)

        imgFlag = findViewById(R.id.imgFlag)
        txtDocType = findViewById(R.id.txtDocType)
        txtEdit = findViewById(R.id.txtEdit)
        txtEdit?.setOnClickListener(editOnClickListener)

        txtInfoAC = findViewById(R.id.txtInfoAC)
        txtInfoAC?.setOnClickListener(infoOnClickListener)


        txtRepeatFront = findViewById(R.id.txtRepeatFront)
        txtRepeatFront?.setOnClickListener(repeatFront)

        txtRepeatBack = findViewById(R.id.txtRepeatBack)
        txtRepeatBack?.setOnClickListener(repeatBack)

        txtachint = findViewById(R.id.txtachint)
        formaText(txtachint!!,"Usa <b><font color='#04D98B' face='Raleway,Bold'>Autocaptura</font></b> para tomar una foto de la parte <b><font color='#04D98B'>delantera</font></b> de tu documento")

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!, "La herramienta de <b><font color='#707070' face='Raleway,Bold'>Autocaptura de TOC</font></b> es un sistema que permite la obtención de fotografías de documentos de identidad desde la cámara de un dispositivo, procurando que las imágenes capturadas estén enfocadas y sean legibles." +
                "<br><br>" +
                "TOC Autocaptura está <b><font color='#707070' face='Raleway,Bold'>disponible para Android, iOS y web</font></b>. Este sistema puede incorporarse y adaptarse al flujo de su aplicación según se requiera. En particular, TOC Autocaptura puede ser utilizada en conjunto con TOC Liveness y la API Facial de TOC para realizar verificaciones de identidad con biometría facial seguras y menos propensas a errores de uso.")

        imgFront = findViewById(R.id.imgFront)

        imgDocSide = findViewById(R.id.imgDocSide)

        boxHints = findViewById(R.id.boxHints)
        boxResult = findViewById(R.id.boxResult)

        btnACnext = findViewById(R.id.btnACnext)
        btnACnext?.setOnClickListener(nextOnClickListener)

        btnACnext?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })


        btnACnext2 = findViewById(R.id.btnACnext2)
        btnACnext2?.setOnClickListener(next2OnClickListener)

        btnACnext2?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnACfront = findViewById(R.id.btnACfront)
        btnACfront?.setOnClickListener(frontOnClickListener)

        btnACfront?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnACback = findViewById(R.id.btnACback)
        btnACback?.setOnClickListener(backOnClickListener)

        btnACback?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnBack = findViewById(R.id.btnBack)
        btnBack?.setOnClickListener(gobackOnClickListener)

        btnClose = findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(closeOnClickListener)
        loadingFrame = findViewById(R.id.loadingFrame)

        setDoctype()
        setFlag()

    }

    private val frontOnClickListener = View.OnClickListener {
        frontSideAutoCapture()

    }

    private val backOnClickListener = View.OnClickListener {
        backSideAutoCapture()

    }

    private val nextOnClickListener = View.OnClickListener {

        formaText(txtachint!!,"Usa <b><font color='#04D98B'>Autocaptura</font></b> para tomar una foto de la parte <b><font color='#04D98B'>trasera</font></b> de tu documento")

        txtRepeatFront?.visibility = View.GONE
        imgFront?.visibility = View.GONE
        btnACfront?.visibility = View.GONE
        btnACnext?.visibility = View.GONE

        boxHints?.visibility = View.VISIBLE
        btnACback?.visibility = View.VISIBLE
        txtachint?.visibility = View.VISIBLE
        imgDocSide?.setImageDrawable(getDrawable(R.drawable.idback))


    }

    private val next2OnClickListener = View.OnClickListener {

        bringPreLiveness()

    }

    private val infoOnClickListener = View.OnClickListener {
        layout_acfront?.visibility = View.GONE
        layout_acinfo?.visibility = View.VISIBLE
    }

    private val gobackOnClickListener = View.OnClickListener {
        layout_acinfo?.visibility = View.GONE
        layout_acfront?.visibility = View.VISIBLE
    }

    private fun consumeSessionManager(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("apiKey", variables.api_key)
                .addFormDataPart("autocapture", "true")
                .addFormDataPart("fake_detector", "true")
                .addFormDataPart("mode", "1")
                .addFormDataPart("liveness", "true")
                .addFormDataPart("liveness_passive", "true").build()
            val request: Request = Request.Builder()
                .url(variables.url_session_manager)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            println("trying to connect")
            resp = response.body()!!.string()
            println("RESPONSE SM: " + resp)
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        StopLoading()
        println("Result Code DEMO: $resultCode")
        if (requestCode == 1) {
            StopLoading()
            if (resultCode == 200) {
                val toc_token = data?.getStringExtra("capture_token")
                println("TOC TOEKEN: " + toc_token)
                //val t = findViewById<TextView>(R.id.texto)
                //t.text = toc_token
                val b64_image = data?.getStringExtra("image")
                //val i = findViewById<ImageView>(R.id.token)
                val image = Base64.decode(b64_image, Base64.URL_SAFE)
                //byte[] image = data.getByteArrayExtra("image");
                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                    println("************Image on DEMO size: WIDTH= " + b.width + " HEIGHT: " + b.height)

                } catch (e: java.lang.Exception) {
                    b = null
                    e.printStackTrace();
                }
                txtachint?.visibility = View.GONE
                btnACfront?.visibility = View.GONE
                txtRepeatBack?.visibility = View.GONE
                boxHints?.visibility = View.GONE


                boxResult?.visibility = View.VISIBLE
                imgFront?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 800, 450, false))
                imgFront?.visibility = View.VISIBLE
                imgFront?.background = getDrawable(R.drawable.rounded_rec)
                imgFront?.clipToOutline = true

                front_token = toc_token

                btnACnext?.visibility = View.VISIBLE
                txtRepeatFront?.visibility = View.VISIBLE

                txtPreFront?.text = "Take back picture of your document"
                /*btnOk?.visibility = View.GONE
                btnBack?.visibility = View.VISIBLE*/

                //i.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))
            }
            if (resultCode == 403) {
                //val image =
                    //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Time is over."

            }
            if (resultCode == 401) {
                //val image =
                    //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Not authorized"
            }
            if (resultCode == 405) {
                //val image =
                    //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Not authorized"
            }
        }
        if (requestCode == 2) {
            StopLoading()
            if (resultCode == 200) {
                StopLoading()
                val toc_token = data?.getStringExtra("capture_token")
                println("TOC TOEKEN: " + toc_token)
                //val t = findViewById<TextView>(R.id.texto)
                //t.text = toc_token
                val b64_image = data?.getStringExtra("image")
                //val i = findViewById<ImageView>(R.id.token)
                val image =
                    Base64.decode(b64_image, Base64.URL_SAFE)
                //byte[] image = data.getByteArrayExtra("image");
                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                    println("************Image on DEMO size: WIDTH= " + b.width + " HEIGHT: " + b.height)
                } catch (e: java.lang.Exception) {
                    b = null
                    e.printStackTrace();
                }


                txtachint?.visibility = View.GONE
                btnACfront?.visibility = View.GONE
                btnACback?.visibility = View.GONE
                btnACnext?.visibility = View.GONE
                txtRepeatFront?.visibility = View.GONE
                boxHints?.visibility = View.GONE


                boxResult?.visibility = View.VISIBLE
                txtRepeatBack?.visibility = View.VISIBLE
                imgFront?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 800, 450, false))
                imgFront?.visibility = View.VISIBLE
                imgFront?.background = getDrawable(R.drawable.rounded_rec_uncolored)

                //front_token = toc_token

                btnACnext2?.visibility = View.VISIBLE
                back_token = toc_token

                //i.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))
            }
            if (resultCode == 403) {
                //val image =
                //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Time is over."
            }
            if (resultCode == 401) {
                //val image =
                //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Not authorized"
            }
            if (resultCode == 405) {
                //val image =
                //findViewById<ImageView>(R.id.profile_image)
                //image.setImageResource(R.drawable.ic_user_male)
                //val texto = findViewById<TextView>(R.id.texto)
                //texto.text = "Not authorized"
            }
        }
        if (requestCode == 3)
        {
            StopLoading()
            if(resultCode == 404)
            {
                setResult(404)
                finish()
            }
        }
    }

    private fun bringPreLiveness(){

        println("FRONT TOKEN: " + front_token)
        println("BACK TOKEN: " + back_token)

        val intent = Intent(this, PreLiveness::class.java)
        intent.putExtra("session_id", session_id)
        intent.putExtra("front_token", front_token)
        intent.putExtra("back_token", back_token)
        intent.putExtra("document_type", document_type)
        startActivityForResult(intent, 3)
    }

    private fun afterSessionId(session_id:String, side: String){
        runOnUiThread {
            StopLoading()
            val intent = Intent(this, CaptureActivity::class.java)
            intent.putExtra("session_id", session_id)
            intent.putExtra("url_wsac", variables.url_wsac)
            intent.putExtra("url_lbac", variables.url_lbac)
            intent.putExtra("document_side", side)
            intent.putExtra("document_type", document_type)
            intent.putExtra("show_flash_button",true)
            startActivityForResult(intent, 1)
        }
    }

    private fun frontSideAutoCapture(){

        runOnUiThread {
            CallLoading()
        }

        val t = Thread(Runnable {
            try {
                val rsp_data = consumeSessionManager()
                val rsp = JSONObject(rsp_data)
                session_id = rsp.getString("session_id")

                afterSessionId(session_id!!, "front")

            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()

    }

    private fun backSideAutoCapture() {

        runOnUiThread {
            StopLoading()
            val intent = Intent(this, CaptureActivity::class.java)
            intent.putExtra("session_id", session_id)
            intent.putExtra("url_wsac", variables.url_wsac)
            intent.putExtra("url_lbac", variables.url_lbac)
            intent.putExtra("document_side", "back")
            intent.putExtra("document_type", document_type)
            intent.putExtra("show_flash_button",true)
            startActivityForResult(intent, 2)

        }
    }

    private val repeatFront = View.OnClickListener {
        frontSideAutoCapture()
    }

    private val repeatBack = View.OnClickListener {

        runOnUiThread {
            CallLoading()
        }
        val t = Thread(Runnable {
            try {
                val rsp_data = consumeSessionManager()
                val rsp = JSONObject(rsp_data)
                session_id = rsp.getString("session_id")

                runOnUiThread {
                    backSideAutoCapture()
                }
            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()


        println("AUTOCAPTURA BACK session_id: '$session_id'")



    }

    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    private val editOnClickListener = View.OnClickListener {
        setResult(405)
        finish()
    }

    private fun setFlag(){

        if(flag == "CHL"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.chl))
        }

        if(flag == "PER")
        {
            imgFlag?.setImageDrawable(getDrawable(R.drawable.per))
        }

        if(flag == "ARG"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.arg))
        }
        if(flag == "MEX"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.mex))
        }


    }

    private fun setDoctype(){
        if(document_type == "CHL2"){
            formaText(txtDocType!!, "Cédula chilena <b>nueva</b>")
        }

        if(document_type == "CHL1")
        {
            formaText(txtDocType!!, "Cédula chilena <b>antigua</b>")
        }

        if(document_type == "PER1")
        {
            formaText(txtDocType!!, "Cédula peruana <b>antigua</b>")
        }

        if(document_type == "PER2")
        {
            formaText(txtDocType!!, "Cédula peruana <b>nueva</b>")
        }

        if(document_type == "PER3")
        {
            formaText(txtDocType!!, "Documento extranjeria")
        }

        if(document_type == "ARG1")
        {
            formaText(txtDocType!!, "Cédula argentina <b>antigua</b>")
        }

        if(document_type == "ARG2")
        {
            formaText(txtDocType!!, "Cédula argentina <b>nueva</b>")
        }

        if(document_type == "MEX1")
        {
            formaText(txtDocType!!, "ID electoral de México(IFE)")
        }

        if(document_type == "MEX2")
        {
            formaText(txtDocType!!, "ID electoral de México(INE)")
        }
    }


    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
           tv.setText(Html.fromHtml(bold));
        }

    }

    private fun CallLoading() {
        loadingFrame?.visibility = View.VISIBLE

    }

    private fun StopLoading() {
        loadingFrame?.visibility = View.GONE

    }

}