package com.example.demo_android

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.hardware.Camera
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.log.fileLogger
import io.fotoapparat.log.logcat
import io.fotoapparat.log.loggers
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.*
import io.fotoapparat.view.CameraView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets


class CameraHandler : AppCompatActivity() {

    private var cameraView: CameraView? = null
    private var camera: Fotoapparat? = null
    private var cont: Context? = null
    private var btnBack: Button? = null
    private var btnTakePic: ImageView? = null
    private var btnTakePic2: ImageView? = null
    private var front_jpg: String? = null
    private var back_jpg: String? = null
    private var imgOverlay: ImageView? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_camera)

        cont = this
        cameraView = findViewById(R.id.nonac_camera)



        val mCamera: Camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK)

        val picture_iterator = mCamera.parameters.supportedPictureSizes.iterator()
        val preview_iterator = mCamera.parameters.supportedPreviewSizes.iterator()


        mCamera.release()

        var picture_height = 0
        var picture_width = 0
        var preview_height = 0
        var preview_width = 0

        picture_iterator.forEach {
            if(it.height == 720 && it.width == 1280){
                picture_height = 720
                picture_width = 1280
            }
        }

        preview_iterator.forEach {
            if(it.height == 720 && it.width == 1280){
                preview_height = 720
                preview_width = 1280
            }
        }

        val cameraConfigurationDef: CameraConfiguration

        if(picture_height != 0 && preview_height != 0){

            cameraConfigurationDef = CameraConfiguration(
                pictureResolution = firstAvailable(
                    { Resolution(picture_width,picture_height) },
                    highestResolution()
                ), // (optional) we want to have the highest possible photo resolution
                previewResolution = firstAvailable(
                    wideRatio({ Resolution(preview_width,preview_height) }),
                    highestResolution()
                ),
                previewFpsRange = highestFps(),          // (optional) we want to have the best frame rate
                focusMode = firstAvailable(
                    continuousFocusPicture(),// (optional) use the first focus mode which is supported by device
                    autoFocus(),// if continuous focus is not available on device, auto focus will be used
                    fixed()                            // if even auto focus is not available - fixed focus mode will be used
                ),
                jpegQuality = manualJpegQuality(90) // (optional) select a jpeg quality of 90 (out of 0-100) values
                          // (optional) receives each frame from preview stream
            )

        }else{
            cameraConfigurationDef = CameraConfiguration(
                pictureResolution = firstAvailable(
                    highestResolution()
                ), // (optional) we want to have the highest possible photo resolution
                previewResolution = firstAvailable(
                    highestResolution()
                ),
                previewFpsRange = highestFps(),          // (optional) we want to have the best frame rate
                focusMode = firstAvailable(
                    // (optional) use the first focus mode which is supported by device
                    continuousFocusPicture(),
                    autoFocus(),// if continuous focus is not available on device, auto focus will be used
                    fixed()                            // if even auto focus is not available - fixed focus mode will be used
                ),
                jpegQuality = manualJpegQuality(90) // (optional) select a jpeg quality of 90 (out of 0-100) values
                          // (optional) receives each frame from preview stream
            )
        }

        camera = Fotoapparat(
            context = this,
            view = cameraView!!,                   // view which will draw the camera preview
            scaleType = ScaleType.CenterCrop,    // (optional) we want the preview to fill the view
            lensPosition = back(),               // (optional) we want back camera
            cameraConfiguration = cameraConfigurationDef, // (optional) define an advanced configuration
            logger = loggers(                    // (optional) we want to log camera events in 2 places at once
                logcat(),                   // ... in logcat
                fileLogger(this)            // ... and to file
            ),
            cameraErrorCallback = { /*error -> PrintError(error)*/}   // (optional) log fatal errors
        )

        btnBack = findViewById(R.id.btnBack)
        btnBack?.setOnClickListener(gobackOnClickListener)

        btnTakePic = findViewById(R.id.btnTakePic)
        btnTakePic?.setOnClickListener(takePicOnClickListener)

        imgOverlay = findViewById(R.id.imgOverlay)

        btnTakePic?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {
                    v?.background = resources.getDrawable(R.drawable.cambutton2)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.cambutton)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnTakePic2 = findViewById(R.id.btnTakePic2)
        btnTakePic2?.setOnClickListener(takePicBackOnClickListener)
        btnTakePic2?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {
                    v?.background = resources.getDrawable(R.drawable.cambutton2)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.cambutton)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnTakePic2?.visibility = View.GONE

        if(intent.getStringExtra("front_jpg") != null && intent.getStringExtra("front_jpg") != ""){

            imgOverlay?.setImageDrawable(getDrawable(R.drawable.backgenerica))
            imgOverlay?.scaleX = -1F
            front_jpg = intent.getStringExtra("front_jpg")
            btnTakePic?.visibility = View.GONE
            btnTakePic2?.visibility = View.VISIBLE
        }


        camera?.start()

    }


    private val gobackOnClickListener = View.OnClickListener {
        camera?.stop()
        setResult(403)
        finish()
    }

    private val takePicOnClickListener = View.OnClickListener {
        imgOverlay?.setImageDrawable(getDrawable(R.drawable.frontgreen))

        val photoResult = camera?.takePicture()
        photoResult
            ?.toBitmap()
            ?.whenAvailable { bitmapPhoto ->

                var stream = ByteArrayOutputStream()
                bitmapPhoto?.bitmap?.compress(Bitmap.CompressFormat.JPEG,90, stream)

                front_jpg = Base64.encodeToString(stream.toByteArray(), Base64.URL_SAFE)

                savePic(front_jpg!!.toByteArray(StandardCharsets.UTF_8),"front")

                val intentPic = Intent(intent)

                camera?.stop()
                setResult(200, intentPic)
                finish()
            }
    }

    private val takePicBackOnClickListener = View.OnClickListener {
        imgOverlay?.setImageDrawable(getDrawable(R.drawable.backgenericagreen))
        imgOverlay?.scaleX = -1F
        val photoResult = camera?.takePicture()
        photoResult
            ?.toBitmap()
            ?.whenAvailable { bitmapPhoto ->

                var stream2 = ByteArrayOutputStream()
                bitmapPhoto?.bitmap?.compress(Bitmap.CompressFormat.JPEG,90, stream2)

                back_jpg = Base64.encodeToString(stream2.toByteArray(), Base64.URL_SAFE)

                savePic(back_jpg!!.toByteArray(StandardCharsets.UTF_8),"back")

                val intentPic2 = Intent(intent)
                //intentPic2.putExtra("back_jpg", back_jpg)
                camera?.stop()
                setResult(200, intentPic2)
                finish()

            }


    }


    private fun savePic(front_bytes: ByteArray, side: String){
        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var filesito = File(myDir, side + ".jpg")
        myDir.mkdirs()

        if (filesito.exists ())
            filesito.delete ()

        try
        {
            val out = FileOutputStream(filesito)
            out.write(front_bytes)
            out.flush()
            out.close()
        }
        catch (e:Exception) {
            e.printStackTrace()
        }
    }



}