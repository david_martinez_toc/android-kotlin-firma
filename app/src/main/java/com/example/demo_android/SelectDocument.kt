package com.example.demo_android


import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cl.toc.labs.autocapture.CaptureActivity
import okhttp3.*
import org.json.JSONObject
import org.w3c.dom.Text
import java.util.concurrent.TimeUnit


class SelectDocument : AppCompatActivity() {

    private var txtCHL1: TextView? = null
    private var txtCHL2: TextView? = null
    private var txtARG1: TextView? = null
    private var txtARG2: TextView? = null
    private var txtMEX1: TextView? = null
    private var txtMEX2: TextView? = null
    private var txtPER1: TextView? = null
    private var txtPER2: TextView? = null
    private var txtPER3: TextView? = null
    private var txtPASS: TextView? = null

    private var btnClose: ImageView? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_select_doc)

        btnClose = findViewById(R.id.btnClose)

        txtCHL1 = findViewById(R.id.txtCHL1)
        txtCHL2 = findViewById(R.id.txtCHL2)
        txtARG1 = findViewById(R.id.txtARG1)
        txtARG2 = findViewById(R.id.txtARG2)
        txtMEX1 = findViewById(R.id.txtMEX1)
        txtMEX2 = findViewById(R.id.txtMEX2)
        txtPER1 = findViewById(R.id.txtPER1)
        txtPER2 = findViewById(R.id.txtPER2)
        txtPER3 = findViewById(R.id.txtPER3)
        txtPASS = findViewById(R.id.txtPASS)

        txtCHL1?.setOnClickListener(CHL1OnClickListener)
        txtCHL2?.setOnClickListener(CHL2OnClickListener)
        txtPER1?.setOnClickListener(PER1OnClickListener)
        txtPER2?.setOnClickListener(PER2OnClickListener)
        txtPER3?.setOnClickListener(PER3OnClickListener)
        txtARG1?.setOnClickListener(ARG1OnClickListener)
        txtARG2?.setOnClickListener(ARG2OnClickListener)
        txtMEX1?.setOnClickListener(MEX1OnClickListener)
        txtMEX2?.setOnClickListener(MEX2OnClickListener)
        txtPASS?.setOnClickListener(PASSOnClickListener)

        formaText(txtCHL2!!, "Cédula chilena <b>nueva</b>")
        formaText(txtCHL1!!, "Cédula chilena <b>antigua</b>")

        formaText(txtARG2!!, "Cédula argentina <b>nueva</b>")
        formaText(txtARG1!!, "Cédula argentina <b>antigua</b>")

        formaText(txtPER2!!, "Cédula peruana <b>nueva</b>")
        formaText(txtPER1!!, "Cédula peruana <b>antigua</b>")

        btnClose?.setOnClickListener(closeOnClickListener)
    }

    private val CHL1OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "CHL")
        intent.putExtra("doc_type", "CHL1")

        startActivityForResult(intent, 200)

    }

    private val CHL2OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "CHL")
        intent.putExtra("doc_type", "CHL2")

        startActivityForResult(intent, 200)

    }

    private val PER1OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "PER")
        intent.putExtra("doc_type", "PER1")

        startActivityForResult(intent, 200)

    }

    private val PER2OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "PER")
        intent.putExtra("doc_type", "PER2")

        startActivityForResult(intent, 200)

    }

    private val PER3OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "PER")
        intent.putExtra("doc_type", "PER3")

        startActivityForResult(intent, 200)

    }

    private val ARG1OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "ARG")
        intent.putExtra("doc_type", "ARG1")
        startActivityForResult(intent, 300)

    }


    private val ARG2OnClickListener = View.OnClickListener {
        val intent = Intent(this, PreFront::class.java)
        intent.putExtra("doc_flag", "ARG")
        intent.putExtra("doc_type", "ARG2")
        startActivityForResult(intent, 300)

    }

    private val MEX1OnClickListener = View.OnClickListener {
        val intent = Intent(this, NonAutocapture::class.java)
        intent.putExtra("doc_flag", "MEX")
        intent.putExtra("doc_type", "MEX1")
        startActivityForResult(intent, 300)

    }


    private val MEX2OnClickListener = View.OnClickListener {
        val intent = Intent(this, NonAutocapture::class.java)
        intent.putExtra("doc_flag", "MEX")
        intent.putExtra("doc_type", "MEX2")
        startActivityForResult(intent, 300)

    }

    private val PASSOnClickListener = View.OnClickListener {
        val intent = Intent(this, Passport::class.java)
        intent.putExtra("doc_flag", "PASS")
        intent.putExtra("doc_type", "PASS")
        startActivityForResult(intent, 300)

    }


    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(resultCode == 404)
        {
            setResult(404)
            finish()
        }

    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }



}