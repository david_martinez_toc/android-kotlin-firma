package com.example.demo_android


import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import androidx.appcompat.app.AppCompatActivity
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import java.io.File
import java.io.FileOutputStream


class SignDocument : AppCompatActivity()  {

    private var data: String? = null

    private var btnFinish: Button? = null
    private var btnContact: ImageView? = null
    private var btnBack: ImageView? = null
    private var txtContact: TextView? = null
    private var toc_token: String? = null
    private var signed_pdf: String? = null

    private var pdf_signed: PDFView? = null
    private var layout_signdoc: View? = null
    private var layout_contact: View? = null

    override fun onBackPressed() {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_signed_document)

        layout_signdoc = findViewById(R.id.layout_signDoc)
        layout_contact = findViewById(R.id.layout_contact)


        btnFinish = findViewById(R.id.btnFinish)
        btnFinish?.setOnClickListener(finishOnClickListener)

        btnContact = findViewById(R.id.btnContact)
        btnContact?.setOnClickListener(contactOnClickListener)


        txtContact = findViewById(R.id.txtContact)
        txtContact?.setOnClickListener(contactOnClickListener)

        btnFinish?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        pdf_signed = findViewById(R.id.pdfView)


        toc_token = intent.getStringExtra("toc_token")
        signed_pdf = intent.getStringExtra("signed_pdf")


        val pdfArray: ByteArray = Base64.decode( signed_pdf, Base64.DEFAULT)
        savePDFtoFile(pdfArray)
        openPDFLibrary()

    }

    private val finishOnClickListener = View.OnClickListener {
        setResult(200)
        finish()
    }

    private fun openPDFLibrary(){

        //var root = Environment.getExternalStorageDirectory().toString()
        var root = this.filesDir.path
        val myDir = File(root)
        var file = File(myDir, "signed.pdf")

        pdf_signed?.useBestQuality(true)
        pdf_signed?.fromFile(file)?.enableAnnotationRendering(true)?.load()


    }

    private val contactOnClickListener = View.OnClickListener {
        layout_signdoc?.visibility = View.GONE
        layout_contact?.visibility = View.VISIBLE

        btnBack = findViewById(R.id.btnBack)

        btnBack?.setOnClickListener(backOnClickListener)

    }

    private val backOnClickListener = View.OnClickListener {
        layout_contact?.visibility = View.GONE
        layout_signdoc?.visibility = View.VISIBLE

    }

    private fun openPDF() {

        val targetPdf = "/sdcard/signed.pdf"

        var file = File(targetPdf)

        println(file.length())

        var fileDescriptor: ParcelFileDescriptor? = null
        fileDescriptor = ParcelFileDescriptor.open(
            file, ParcelFileDescriptor.MODE_READ_ONLY)
        //min. API Level 21
        var pdfRenderer: PdfRenderer? = null
        pdfRenderer = PdfRenderer(fileDescriptor)

        var pageCount = pdfRenderer.getPageCount()

        //Display page 0
        var rendererPage = pdfRenderer.openPage(0)
        var rendererPageWidth = rendererPage.getWidth()
        var rendererPageHeight = rendererPage.getHeight()
        var bitmap2 = Bitmap.createBitmap(
            rendererPageWidth,
            rendererPageHeight,
            Bitmap.Config.ARGB_8888)
        rendererPage.render(bitmap2, null, null,
            PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)

        println("ByteCount " + bitmap2.byteCount)

        rendererPage.close()
        pdfRenderer.close()
        fileDescriptor.close()
    }

    private fun savePDFtoFile(pdf_bytes: ByteArray){

        //var root = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)?.path

        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var filesito = File(myDir, "signed.pdf")
        myDir.mkdirs()

        if (filesito.exists ())
            filesito.delete ()

        try
        {
            val out = FileOutputStream(filesito)
            out.write(pdf_bytes)
            out.flush()
            out.close()
        }
        catch (e:Exception) {
            e.printStackTrace()
        }


    }
}