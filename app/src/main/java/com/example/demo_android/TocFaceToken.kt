package com.example.demo_android

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import cl.toc.labs.tcam.LivenessIntro
import com.github.barteksc.pdfviewer.PDFView
import okhttp3.*
import org.json.JSONObject
import java.io.File
import java.io.InputStream
import java.util.concurrent.TimeUnit


class TocFaceToken : AppCompatActivity()  {

    private var layout_docSign: View? = null
    private var layout_liv: View? = null
    private var layout_livinfo: View? = null

    private var btnSelfie: Button? = null
    private var btnBack: ImageView? = null
    private var session_id: String? = null

    private var front_token: String? = null
    private var back_token: String? = null
    private var liveness_token: String? = null
    private var toc_token: String? = null


    private var imgLiv: ImageView? = null
    private var txtRepeatLiv: TextView? = null
    private var txtLivhint2: TextView? = null
    private var txtInfoFaceToken: TextView? = null
    private var txtInfoLiv: TextView? = null
    private var txt1st: TextView? = null

    private var btnCheckSign: Button? = null

    private var status: String? = null
    private var biometric_result: String? = null

    private var bio_result_final: String? = null
    private var toc_token_final: String? = null

    private var data: String? = null
    private var signed_pdf: String? = null

    private var imgPDF: PDFView? = null
    private var noAC: String? = null

    private var loadingFrame: FrameLayout? = null


    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_toc_face_token)

        layout_docSign = findViewById(R.id.layout_docSign)
        layout_liv = findViewById(R.id.layout_liv)
        layout_livinfo = findViewById(R.id.layout_livinfo)

        imgPDF = findViewById(R.id.imgPDF)

        imgLiv = findViewById(R.id.imgLiv)

        txtRepeatLiv = findViewById(R.id.txtRepeatLiv)
        txtRepeatLiv?.setOnClickListener(repeatLiv)

        txtInfoLiv = findViewById(R.id.txtInfoLiv)
        txtInfoLiv?.setOnClickListener(infoOnClickListener)

        btnSelfie = findViewById(R.id.btnSelfie)
        btnSelfie?.setOnClickListener(selfieOnClickListener)
        btnSelfie?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })


        btnCheckSign = findViewById(R.id.btnCheckSign)
        btnCheckSign?.setOnClickListener(checkSignOnClickListener)
        btnCheckSign?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })


        front_token = intent.getStringExtra("id_front")
        back_token = intent.getStringExtra("id_back")
        liveness_token = intent.getStringExtra("selfie")
        toc_token = intent.getStringExtra("toc_token")

        if(intent.getStringExtra("noAC") != null && intent.getStringExtra("noAC") == "true")
        {
            noAC = intent.getStringExtra("noAC")
            var fileFront: ByteArray = openPic("front").readBytes()
            var fileBack: ByteArray = openPic("back").readBytes()
            front_token = fileFront.toString(Charsets.UTF_8)
            back_token = fileBack.toString(Charsets.UTF_8)

        }

        btnBack = findViewById(R.id.btnBack)
        btnBack?.setOnClickListener(gobackOnClickListener)

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!, "La herramienta de <b><font color='#707070' face='Raleway,Bold'>Liveness Detection de TOC</font></b> es un sistema que permite la obtención de selfies seguras desde la cámara de un dispositivo, resguardando que las fotografías hayan sido tomadas a una “persona viva” y no se trate, por ejemplo, de una foto de otra foto.")


        loadingFrame = findViewById(R.id.loadingFrame)

        openPDFLibrary()

    }

    private val selfieOnClickListener = View.OnClickListener {

        livenessProcess()
    }

    private val repeatLiv = View.OnClickListener {
        livenessProcess()
    }

    private val checkSignOnClickListener = View.OnClickListener {

        signDocument()
    }

    private fun livenessProcess(){

        runOnUiThread {
            CallLoading()
        }
        val t = Thread(Runnable {
            try {
                val rsp_data = consumeSessionManager()
                val rsp = JSONObject(rsp_data)
                session_id = rsp.getString("session_id")
                runOnUiThread {
                    StopLoading()
                    val intent = Intent(this, LivenessIntro::class.java)
                    intent.putExtra("session_id", session_id)
                    intent.putExtra("url_wsliv", variables.url_wsliv)
                    intent.putExtra("url_lbliv", variables.url_lbliv)
                    startActivityForResult(intent, 1)
                }

            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()


    }

    private fun consumeSessionManager(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("apiKey", variables.api_key)
                .addFormDataPart("mode", "3")
                .addFormDataPart("liveness_passive", "true").build();
            val request: Request = Request.Builder()
                .url(variables.url_session_manager)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val toc_token = data!!.getStringExtra("liveness_token")
                val b64_image = data?.getStringExtra("image")

                layout_docSign?.visibility = View.GONE
                layout_liv?.visibility = View.VISIBLE

                println("TOC TOKEN: $toc_token")


                liveness_token = toc_token

                val image = Base64.decode(b64_image, Base64.URL_SAFE)
                //byte[] image = data.getByteArrayExtra("image");
                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                    println("************Image on DEMO size: WIDTH= " + b.width + " HEIGHT: " + b.height)

                } catch (e: java.lang.Exception) {
                    b = null
                    e.printStackTrace();
                }

                imgLiv?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 250, 600, false))
                imgLiv?.visibility = View.VISIBLE
                imgLiv?.background = getDrawable(R.drawable.rounded_rec)
                imgLiv?.clipToOutline = true


                //bringBioResult()

                //i.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                println("RESULT CANCELED ON DEMO")

            }

            if (resultCode == 401) {

            }

            if (resultCode == 405) {

            }
        }

        if(requestCode == 9){
            if(resultCode == 200)
            {
                setResult(404)
                finish()
            }
        }
    }

    private fun openPDFLibrary(){

        /*var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, "testPDF_es.pdf")*/

        imgPDF?.useBestQuality(true)

        imgPDF?.fromAsset("testPDF_es.pdf")?.load()
        //imgPDF?.fromFile(file)?.enableAnnotationRendering(true)?.load()


    }

    private fun signDocument(){

        runOnUiThread {
            CallLoading()
        }

        val t = Thread(Runnable {
            try {
                val rsp_data = consumeFacevToken()
                val rsp = JSONObject(rsp_data)
                data = rsp.toString()


                status = rsp.getString("status")
                bio_result_final = rsp.getString("biometric result")
                toc_token_final = rsp.getString("toc_token")

                runOnUiThread {
                    var pdfFile: InputStream


                    pdfFile = assets.open("testPDF_es.pdf")
                    val pdf_bytes = ByteArray(pdfFile.available())
                    pdfFile.read(pdf_bytes)



                    val t2 = Thread(Runnable {
                        try {
                            var resp = ""
                            resp = signPDF(toc_token_final, pdf_bytes)!!
                            val pdf_json = JSONObject(resp)
                            toc_token = pdf_json.getString("toc_token")
                            signed_pdf = pdf_json.getString("signed pdf")

                            runOnUiThread {
                                StopLoading()
                                pdfFile.close()


                                val intent = Intent(this, SignDocument::class.java)
                                intent.putExtra("toc_token", toc_token)
                                intent.putExtra("signed_pdf", signed_pdf)

                                startActivityForResult(intent, 9)
                            }

                        } catch (e: java.lang.Exception) {
                            e.printStackTrace();
                        }
                    })
                    t2.start()

                }

            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()




    }

    private fun consumeFacevToken(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("photo", liveness_token)
                .addFormDataPart("toc_token", toc_token)
                .addFormDataPart("apiKey", variables.api_key).build()

            val request: Request = Request.Builder()
                .url(variables.url_face_vs_token)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()

            System.out.println("Ans: " + resp);
        } catch (e: Exception) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return resp

    }

    private fun signPDF(trx: String?, pdf: ByteArray): String? {
        CallLoading()
        var resp = ""
        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
        try {
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(
                    "pdf",
                    "pdf",
                    RequestBody.create(MediaType.parse("text/csv"), pdf)
                )
                .addFormDataPart("toc_token", trx)
                .addFormDataPart("page", "1")
                .addFormDataPart("pos_x", "180")
                .addFormDataPart("pos_y", "120")
                .addFormDataPart("apiKey", variables.api_key).build()
            val request: Request = Request.Builder()
                .url(variables.url_simpleSign)
                .post(body)
                .addHeader(
                    "content-type",
                    "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
                )
                .addHeader("cache-control", "no-cache")
                .build()
            val response = client.newCall(request).execute()
            println("trying to connect")
            resp = response.body()!!.string()
            println("WS Resp: $resp")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return resp
    }

    private val infoOnClickListener = View.OnClickListener {
        layout_liv?.visibility = View.GONE
        layout_livinfo?.visibility = View.VISIBLE
    }

    private val gobackOnClickListener = View.OnClickListener {
        layout_livinfo?.visibility = View.GONE
        layout_liv?.visibility = View.VISIBLE
    }

    private fun signFEAPDF(trx: String?, pdf: ByteArray): String? {
        println("PDf length: " + pdf.size)
        var resp = ""
        val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
        try {
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(
                    "pdf",
                    "pdf",
                    RequestBody.create(MediaType.parse("text/csv"), pdf)
                )
                .addFormDataPart("toc_token", trx)
                .addFormDataPart("page", "1")
                .addFormDataPart("pos_x", "180")
                .addFormDataPart("pos_y", "120")
                .addFormDataPart("apiKey", variables.api_key).build()
            val request: Request = Request.Builder()
                .url(variables.url_FEA)
                .post(body)
                .addHeader(
                    "content-type",
                    "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
                )
                .addHeader("cache-control", "no-cache")
                .build()
            val response = client.newCall(request).execute()
            println("trying to connect")
            resp = response.body()!!.string()
            println("WS Resp: $resp")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return resp
    }

    private fun openPic(side: String) : File {
        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, side + ".jpg")

        return file
    }

    private fun CallLoading() {
        loadingFrame?.visibility = View.VISIBLE

    }

    private fun StopLoading() {
        loadingFrame?.visibility = View.GONE

    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }

}