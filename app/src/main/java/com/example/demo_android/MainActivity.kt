package com.example.demo_android


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
//import androidx.appcompat.app.ActivityCompat
//import androidx.appcompat.content.ContextCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var btnStart: Button? = null
    private var btnClose: ImageView? = null
    private var btnInfo: TextView? = null
    private var btnMail: ImageView? = null
    private var btnBack: ImageView? = null
    private var txt1st: TextView? = null
    private var layout_demo: View? = null
    private var layout_contact: View? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                1
            )
        }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
        }

        btnStart = findViewById(R.id.btnStart)
        btnClose = findViewById(R.id.btnClose)
        btnInfo = findViewById(R.id.btnInfo)
        btnMail = findViewById(R.id.btnMail)

        layout_demo = findViewById(R.id.layout_demo)
        layout_contact = findViewById(R.id.layout_contact)


        btnStart?.setOnClickListener(startOnClickListener)
        btnClose?.setOnClickListener(closeOnClickListener)
        btnInfo?.setOnClickListener(infoOnClickListener)
        btnMail?.setOnClickListener(infoOnClickListener)


        btnStart?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnClose?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.alpha = .5F
                    v?.scaleX = 1.5F
                    v?.scaleY = 1.5F
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.alpha = 1f
                    v?.scaleX = 1F
                    v?.scaleY = 1F
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnMail?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.alpha = .5F
                    v?.scaleX = 1.1F
                    v?.scaleY = 1.1F
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.alpha = 1f
                    v?.scaleX = 1F
                    v?.scaleY = 1F
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!, "Esta es una aplicación de demostración con la que podrás realizar verificación de identidad y firmar documentos electrónicamente, usando las diferentes tecnologías TOC." +
                "<br><br>El proceso de <b><font color='#707070' face='Raleway,Bold'>verificación de identidad</font></b> consta de dos sencillos pasos:" +
                "<br>" +
                "<br><b><font color='#707070' face='Raleway,Bold'>Captura una imagen</font></b> de la parte delantera y trasera de tu <b><font color='#707070' face='Raleway'>documento de identidad</font></b> para extraer datos." +
                "<br>" +
                "<br><b><font color='#707070' face='Raleway,Bold'>Toma una foto de tu rostro</font></b> para hacer biometría." +
                "<br>" +
                "<br>Una vez efectuada la verificación de identidad, podrás realizar la <b><font color='#707070' face='Raleway,Bold'>firma electrónica</font></b> de un documento.")


    }

    private val startOnClickListener = View.OnClickListener {
        val intent = Intent(this, SelectDocument::class.java)
        startActivityForResult(intent, 100)
    }


    private val closeOnClickListener = View.OnClickListener {
        finish()
    }

    private val infoOnClickListener = View.OnClickListener {
       layout_demo?.visibility = View.GONE
        layout_contact?.visibility = View.VISIBLE

        btnBack = findViewById(R.id.btnBack)

        btnBack?.setOnClickListener(backOnClickListener)

    }

    private val backOnClickListener = View.OnClickListener {
        layout_contact?.visibility = View.GONE
        layout_demo?.visibility = View.VISIBLE

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {



    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }


}
