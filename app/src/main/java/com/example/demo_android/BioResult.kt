package com.example.demo_android

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import android.text.Html
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import okhttp3.*
import org.json.JSONObject
import java.io.File
import java.util.concurrent.TimeUnit

class BioResult : AppCompatActivity() {

    private var front_token: String? = null
    private var back_token: String? = null
    private var liveness_token: String? = null

    private var status: String? = null
    private var biometric_result: String? = null
    private var toc_token: String? = null


    private var rut: String? = null
    private var name: String? = null
    private var lastName: String? = null
    private var gender: String? = null
    private var nationality: String? = null
    private var expiration: String? = null
    private var doc_number: String? = null
    private var birth_place: String? = null
    private var birth_date: String? = null
    private var occupation: String? = null


    private var imgCheck: ImageView? = null
    private var imgProfilePic: ImageView? = null


    private var layout_result: View? = null
    private var layout_details: View? = null
    private var layout_bioInfo: View? = null

    private var txtInfoBio: TextView? = null
    private var txtHow: TextView? = null
    private var txtBioResult: TextView? = null
    private var txtFullDetail: TextView? = null
    private var txt1st: TextView? = null

    private var txtRut: TextView? = null
    private var txtName: TextView? = null
    private var txtLastName: TextView? = null

    private var txtRut_result: TextView? = null
    private var txtName_result: TextView? = null
    private var txtLastName_result: TextView? = null
    private var txtGender_result: TextView? = null
    private var txtNationality_result: TextView? = null
    private var txtBirthPlace_result: TextView? = null
    private var txtBirthDate_result: TextView? = null
    private var txtOccupation_result: TextView? = null



    private var btnAPI: Button? = null
    private var btnClose: ImageView? = null
    private var btnBack: ImageView? = null
    private var btnBack2: ImageView? = null
    private var btnInfoAPI: ImageView? = null

    private var imgSelfie: String? = null

    private var document_type: String? = null


    private var noAC: String? = null
    private var passport: String? = null
    private var loadingFrame: FrameLayout? = null
    private var alertdialog: AlertDialog? = null
    private var json: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_bio_result)

        txtBioResult = findViewById(R.id.txtBioResult)
        imgCheck = findViewById(R.id.imgCheck)
        loadingFrame = findViewById(R.id.loadingFrame)

        front_token = intent.getStringExtra("id_front")
        back_token = intent.getStringExtra("id_back")
        liveness_token = intent.getStringExtra("selfie")
        imgSelfie = intent.getStringExtra("imgSelfie")
        document_type = intent.getStringExtra("document_type")
        json = intent.getStringExtra("json")


        if(intent.getStringExtra("noAC") != null && intent.getStringExtra("noAC") == "true")
        {
            noAC = intent.getStringExtra("noAC")

            if(intent.getStringExtra("passport") != null && intent.getStringExtra("passport") == "true")
            {
                passport = intent.getStringExtra("passport")
                var fileFront: ByteArray = openPic("front").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)

            }else
            {

                var fileFront: ByteArray = openPic("front").readBytes()
                var fileBack: ByteArray = openPic("back").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)
                back_token = fileBack.toString(Charsets.UTF_8)

            }


        }


        layout_result = findViewById(R.id.layout_result)
        layout_bioInfo = findViewById(R.id.layout_APInfo)
        layout_details = findViewById(R.id.layout_details)



        txtInfoBio = findViewById(R.id.txtInfoBio)
        txtInfoBio?.setOnClickListener(infoBioOnClickListener)

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!, "La <b><font color='#707070' face='Raleway, Bold'>API Facial TOC</font></b> permite procesar selfies e imágenes de distintos tipos de documentos de identidad, como documentos nacionales de identidad, licencias de conducir, pasaportes, entre otros. Para así obtener los datos personales y verificar la identidad.")

        txtBirthDate_result = findViewById(R.id.txtBirthDate_result)
        txtBirthPlace_result = findViewById(R.id.txtBirthPlace_result)
        txtFullDetail = findViewById(R.id.txtFullDetail)
        txtGender_result = findViewById(R.id.txtGender_result)
        txtHow = findViewById(R.id.txtHow)
        txtLastName = findViewById(R.id.txtLastName)
        txtLastName_result = findViewById(R.id.txtLastName_result)
        txtName = findViewById(R.id.txtName)
        txtName_result = findViewById(R.id.txtName_result)
        txtNationality_result = findViewById(R.id.txtNat_result)
        txtOccupation_result = findViewById(R.id.txtOccupation_result)
        txtRut = findViewById(R.id.txtRUT)
        txtRut_result = findViewById(R.id.txtRUT_result)


        btnAPI = findViewById(R.id.btnAPI)
        btnAPI?.setOnClickListener(apiOnClickListener)
        btnAPI?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })


        btnClose = findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(closeOnClickListener)

        btnBack = findViewById(R.id.btnBack)
        btnBack2 = findViewById(R.id.btnBack2)

        btnBack?.setOnClickListener(gobackOnClickListener)
        btnBack2?.setOnClickListener(goback2OnClickListener)

        txtFullDetail?.setOnClickListener(showDetailsOnClickListener)

        imgProfilePic = findViewById(R.id.imgProfilePic)

        //getBioResult()

        retrieveJsonData(json!!)

    }

    private fun retrieveJsonData(json: String){

       var rsp = JSONObject(json)

        status = rsp?.getString("status")


        if(status != "200")
        {
            txtBioResult?.text = "NEGATIVO"
            txtBioResult?.setTextColor(Color.parseColor("#FF2943"))
            imgCheck?.setImageDrawable(getDrawable(R.drawable.checke))
            btnAPI?.isEnabled = false

            val alertdiag = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom))
            alertdiag.setMessage("Verificación fue negativa. Intentelo nuevamente")
                .setPositiveButton("Cerrar") { dialog, which ->
                    //FinishCamera()
                }

            runOnUiThread {
                alertdialog = alertdiag.create()
                alertdialog?.setCanceledOnTouchOutside(false)
                alertdialog?.show()
            }

        }else
        {
            biometric_result = rsp?.getString("biometric result")
            toc_token = rsp?.getString("toc_token")


            var info_doc = rsp?.getJSONObject("information from document")

            var code:JSONObject? = null
            var data:JSONObject? = null
            var mrz:JSONObject? = null

            if(document_type != "PASS" && document_type != "CHL1" && document_type != "CHL2")
            {
                code = info_doc?.getJSONObject("code")

            }else
            {
                mrz = info_doc?.getJSONObject("mrz")
            }

            if(document_type != "CHL2" && document_type != "CHL1" && document_type != "PASS")
            {
                data = code?.getJSONObject("data")
                mrz = info_doc?.getJSONObject("mrz")

                val data_mrz = mrz?.getJSONObject("data")

                rut = data?.getString("national identification number")
                name = data?.getString("name")
                lastName = data?.getString("family name")
                gender = data?.getString("gender")
                birth_date = data_mrz?.getString("date of birth")
                expiration = data_mrz?.getString("expiration date")
                doc_number = data_mrz?.getString("document number")
                nationality = data_mrz?.getString("nationality")
            }
            else if(document_type == "PASS")
            {
                data = mrz?.getJSONObject("data")
                rut = ""
                name = data?.getString("name")
                lastName = data?.getString("family name")
                gender = data?.getString("gender")
                birth_date = data?.getString("date of birth")
                expiration = data?.getString("expiration date")
                doc_number = data?.getString("document number")
                nationality = data?.getString("nationality")

            }
            else
            {
                mrz = info_doc?.getJSONObject("mrz")
                data = mrz?.getJSONObject("data")

                rut = data?.getString("national identification number")
                name = data?.getString("name")
                lastName = data?.getString("family name")
                gender = data?.getString("gender")
                birth_date = data?.getString("date of birth")
                expiration = data?.getString("expiration date")
                doc_number = data?.getString("document number")
                nationality = data?.getString("nationality")
            }


            println(data)

        }


        if(biometric_result == "2")
        {
            txtBioResult?.text = "POSITIVO"
            imgCheck?.setImageDrawable(getDrawable(R.drawable.check))
        }

        if(biometric_result == "1")
        {
            txtBioResult?.text = "POSITIVO"
            txtBioResult?.setTextColor(Color.parseColor("#D9B604"))
            imgCheck?.setImageDrawable(getDrawable(R.drawable.checky))
        }

        if(biometric_result == "0" || biometric_result == "-1")
        {
            txtBioResult?.text = "NEGATIVO"
            txtBioResult?.setTextColor(Color.parseColor("#FF2943"))
            imgCheck?.setImageDrawable(getDrawable(R.drawable.checke))
            btnAPI?.isEnabled = false
            val alertdiag = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom))
            alertdiag.setMessage("Verificación fue negativa. Intentelo nuevamente")
                .setPositiveButton("Cerrar") { dialog, which ->
                    //FinishCamera()
                }
            runOnUiThread {
                alertdialog = alertdiag.create()
                alertdialog?.setCanceledOnTouchOutside(false)
                alertdialog?.show()
            }
        }


        txtRut?.text = rut
        txtRut_result?.text = rut
        txtName?.text = name
        txtName_result?.text = name
        txtLastName?.text = lastName
        txtLastName_result?.text = lastName
        txtGender_result?.text = gender
        txtNationality_result?.text = nationality
        txtBirthDate_result?.text = birth_date


        val image = Base64.decode(imgSelfie, Base64.URL_SAFE)
        var b: Bitmap?
        b = BitmapFactory.decodeByteArray(image, 0, image.size)
        imgProfilePic?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 450, 800, false))
        imgProfilePic?.background = getDrawable(R.drawable.rounded_rec)
        imgProfilePic?.scaleType = ImageView.ScaleType.FIT_XY
        imgProfilePic?.clipToOutline = true


    }

    private fun getBioResult(){

        runOnUiThread {
            CallLoading()
        }

        val t = Thread(Runnable {
            try {
                val rsp_data: String?

                if(noAC == "true")
                {
                    rsp_data = consumeNoACTocApi()
                }else
                {
                    rsp_data = consumeTocApi()
                }

                val rsp = JSONObject(rsp_data)
                println(rsp.toString())




                afterThread(rsp)


            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()


    }

    private fun afterThread(rsp: JSONObject){

        runOnUiThread {
            StopLoading()

            status = rsp.getString("status")


            if(status != "200")
            {
                txtBioResult?.text = "NEGATIVO"
                txtBioResult?.setTextColor(Color.parseColor("#FF2943"))
                imgCheck?.setImageDrawable(getDrawable(R.drawable.checke))
                btnAPI?.isEnabled = false

                val alertdiag = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom))
                alertdiag.setMessage("Verificación fue negativa. Intentelo nuevamente")
                    .setPositiveButton("Cerrar") { dialog, which ->
                        //FinishCamera()
                    }

                runOnUiThread {
                    alertdialog = alertdiag.create()
                    alertdialog?.setCanceledOnTouchOutside(false)
                    alertdialog?.show()
                }

            }else
            {
                biometric_result = rsp.getString("biometric result")
                toc_token = rsp.getString("toc_token")


                var info_doc = rsp.getJSONObject("information from document")

                var code:JSONObject? = null
                var data:JSONObject? = null
                var mrz:JSONObject? = null

                if(document_type != "PASS" && document_type != "CHL1" && document_type != "CHL2")
                {
                    code = info_doc.getJSONObject("code")

                }else
                {
                    mrz = info_doc.getJSONObject("mrz")
                }

                if(document_type != "CHL2" && document_type != "CHL1" && document_type != "PASS")
                {
                    data = code?.getJSONObject("data")
                    mrz = info_doc.getJSONObject("mrz")

                    val data_mrz = mrz.getJSONObject("data")

                    rut = data?.getString("national identification number")
                    name = data?.getString("name")
                    lastName = data?.getString("family name")
                    gender = data?.getString("gender")
                    birth_date = data_mrz.getString("date of birth")
                    expiration = data_mrz.getString("expiration date")
                    doc_number = data_mrz.getString("document number")
                    nationality = data_mrz.getString("nationality")
                }
                else if(document_type == "PASS")
                {
                    data = mrz?.getJSONObject("data")
                    rut = ""
                    name = data?.getString("name")
                    lastName = data?.getString("family name")
                    gender = data?.getString("gender")
                    birth_date = data?.getString("date of birth")
                    expiration = data?.getString("expiration date")
                    doc_number = data?.getString("document number")
                    nationality = data?.getString("nationality")

                }
                else
                {
                    mrz = info_doc.getJSONObject("mrz")
                    data = mrz.getJSONObject("data")

                    rut = data.getString("national identification number")
                    name = data.getString("name")
                    lastName = data.getString("family name")
                    gender = data.getString("gender")
                    birth_date = data.getString("date of birth")
                    expiration = data.getString("expiration date")
                    doc_number = data.getString("document number")
                    nationality = data.getString("nationality")
                }


                println(data)



            }


            if(biometric_result == "2")
            {
                txtBioResult?.text = "POSITIVO"
                imgCheck?.setImageDrawable(getDrawable(R.drawable.check))
            }

            if(biometric_result == "1")
            {
                txtBioResult?.text = "POSITIVO"
                txtBioResult?.setTextColor(Color.parseColor("#D9B604"))
                imgCheck?.setImageDrawable(getDrawable(R.drawable.checky))
            }

            if(biometric_result == "0" || biometric_result == "-1")
            {
                txtBioResult?.text = "NEGATIVO"
                txtBioResult?.setTextColor(Color.parseColor("#FF2943"))
                imgCheck?.setImageDrawable(getDrawable(R.drawable.checke))
                btnAPI?.isEnabled = false
                val alertdiag = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom))
                alertdiag.setMessage("Verificación fue negativa. Intentelo nuevamente")
                    .setPositiveButton("Cerrar") { dialog, which ->
                        //FinishCamera()
                    }
                runOnUiThread {
                    alertdialog = alertdiag.create()
                    alertdialog?.setCanceledOnTouchOutside(false)
                    alertdialog?.show()
                }
            }


            txtRut?.text = rut
            txtRut_result?.text = rut
            txtName?.text = name
            txtName_result?.text = name
            txtLastName?.text = lastName
            txtLastName_result?.text = lastName
            txtGender_result?.text = gender
            txtNationality_result?.text = nationality
            txtBirthDate_result?.text = birth_date


            val image = Base64.decode(imgSelfie, Base64.URL_SAFE)
            var b: Bitmap?
            b = BitmapFactory.decodeByteArray(image, 0, image.size)
            imgProfilePic?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 450, 800, false))
            imgProfilePic?.background = getDrawable(R.drawable.rounded_rec)
            imgProfilePic?.scaleType = ImageView.ScaleType.FIT_XY
            imgProfilePic?.clipToOutline = true
        }


    }

    private fun consumeTocApi(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("id_front", front_token!!)
                .addFormDataPart("id_back", back_token!!)
                .addFormDataPart("selfie", liveness_token!!)
                .addFormDataPart("documentType", document_type!!)
                .addFormDataPart("apiKey", variables.api_key).build()

            val request: Request = Request.Builder()
                .url(variables.url_face_vs_doc)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }

    private fun consumeNoACTocApi(): String?{
        var resp = ""
        val b1: ByteArray = Base64.decode(front_token, Base64.URL_SAFE)
        val b2: ByteArray = Base64.decode(back_token, Base64.URL_SAFE)
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

            var body: RequestBody? = null
            if(passport != "true")
            {
                body = MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("id_front", "frontal", RequestBody.create(MediaType.parse("image/jpeg"), b1))
                    .addFormDataPart("id_back", "trasera", RequestBody.create(MediaType.parse("image/jpeg"), b2))
                    .addFormDataPart("selfie", liveness_token)
                    .addFormDataPart("documentType", document_type)
                    .addFormDataPart("apiKey", variables.api_key).build()
            }else
            {
                body = MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("id_front", "frontal", RequestBody.create(MediaType.parse("image/jpeg"), b1))
                    .addFormDataPart("selfie", liveness_token)
                    .addFormDataPart("documentType", document_type)
                    .addFormDataPart("apiKey", variables.api_key).build()
            }


            val request: Request = Request.Builder()
                .url(variables.url_face_vs_doc)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 5)
        {
            if(resultCode == 404)
            {
                setResult(404)
                finish()
            }
        }
    }


    private val apiOnClickListener = View.OnClickListener {
        val intent = Intent(this, TocApi::class.java)

        if(noAC == "true")
        {
            intent.putExtra("noAC", "true")
            intent.putExtra("id_front", "ac")
            intent.putExtra("id_back", "ac")

            if(passport == "true")
            {
                intent.putExtra("passport", "true")
            }
        }else
        {
            intent.putExtra("id_front", front_token)
            intent.putExtra("id_back", back_token)
        }


        intent.putExtra("selfie", liveness_token)
        intent.putExtra("documentType", document_type)
        intent.putExtra("apiKey", variables.api_key)
        intent.putExtra("toc_token", toc_token)
        intent.putExtra("document_type", document_type)

        startActivityForResult(intent,5)

    }

    private val infoBioOnClickListener = View.OnClickListener {
        layout_result?.visibility = View.GONE
        layout_bioInfo?.visibility = View.VISIBLE
    }

    private val showDetailsOnClickListener = View.OnClickListener {
        layout_result?.visibility = View.GONE
        layout_bioInfo?.visibility = View.GONE
        layout_details?.visibility = View.VISIBLE
    }

    private val gobackOnClickListener = View.OnClickListener {
        layout_bioInfo?.visibility = View.GONE
        layout_details?.visibility = View.GONE
        layout_result?.visibility = View.VISIBLE
    }

    private val goback2OnClickListener = View.OnClickListener {
        layout_bioInfo?.visibility = View.GONE
        layout_details?.visibility = View.GONE
        layout_result?.visibility = View.VISIBLE
    }

    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    private fun openPic(side: String) : File {
        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, side + ".jpg")

        return file
    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }

    private fun CallLoading() {
        loadingFrame?.visibility = View.VISIBLE

    }

    private fun StopLoading() {
        loadingFrame?.visibility = View.GONE

    }
}