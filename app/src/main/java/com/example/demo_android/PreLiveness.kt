package com.example.demo_android

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.*
import cl.toc.labs.autocapture.CaptureActivity
import cl.toc.labs.tcam.LivenessIntro
import kotlinx.android.synthetic.main.layout_pre_liveness.*
import okhttp3.*
import org.json.JSONObject
import java.io.File
import java.util.concurrent.TimeUnit

class PreLiveness : AppCompatActivity() {

    private var session_id: String? = null
    private var btnLiv: Button? = null
    private var btnBack: ImageView? = null
    private var btnCheck: Button? = null
    private var btnClose: ImageView? = null
    private var txtRepeatLiv: TextView? = null

    private var layout_liv: View? = null
    private var layout_livinfo: View? = null
    private var txtInfoLiv: TextView? = null
    private var txtLivhint: TextView? = null
    private var txt1st: TextView? = null




    private var front_token: String? = null
    private var back_token: String? = null
    private var liveness_token: String? = null

    private var imgLiv: ImageView? = null
    private var noAC: String? = null

    private var boxHints: View? = null
    private var boxResult: View? = null

    private var imgSelfie: String? = null
    private var blank_result: LinearLayout? = null

    private var document_type: String? = null
    private var passport: String? = null

    private var loadingFrame: FrameLayout? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_pre_liveness)

        btnLiv = findViewById(R.id.btnLiv)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                1
            )
        }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
        }



        session_id = intent.getStringExtra("session_id")


        if(session_id == null || session_id == "")
        {
            val t = Thread(Runnable {
                try {
                    val rsp_data = consumeSessionManager()
                    val rsp = JSONObject(rsp_data)
                    session_id = rsp.getString("session_id")
                } catch (e: java.lang.Exception) { e.printStackTrace();
                }
            })
            t.start()
            try {
                t.join()
            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        }

        front_token = intent.getStringExtra("front_token")
        back_token = intent.getStringExtra("back_token")

        if(intent.getStringExtra("noAC") != null && intent.getStringExtra("noAC") == "true")
        {
            noAC = intent.getStringExtra("noAC")

            if(intent.getStringExtra("passport") != null && intent.getStringExtra("passport") == "true")
            {
                passport = intent.getStringExtra("passport")
                var fileFront: ByteArray = openPic("front").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)
            }else
            {

                var fileFront: ByteArray = openPic("front").readBytes()
                var fileBack: ByteArray = openPic("back").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)
                back_token = fileBack.toString(Charsets.UTF_8)

            }

        }

        document_type = intent.getStringExtra("document_type")



        layout_liv = findViewById(R.id.layout_liv)
        layout_livinfo = findViewById(R.id.layout_livinfo)

        txtInfoLiv = findViewById(R.id.txtInfoLiv)
        txtInfoLiv?.setOnClickListener(infoOnClickListener)

        txtLivhint = findViewById(R.id.txtLivhint)
        formaText(txtLivhint!!, "Usa <b><font color='#04D98B'>Liveness Detection</font></b><br>para obtener una captura<br>segura de tu rostro")

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!, "La herramienta de <b><font color='#707070' face='Raleway,Bold'>Liveness Detection de TOC</font></b> es un sistema que permite la obtención de selfies seguras desde la cámara de un dispositivo, resguardando que las fotografías hayan sido tomadas a una “persona viva” y no se trate, por ejemplo, de una foto de otra foto.")

        txtRepeatLiv = findViewById(R.id.txtRepeatLiv)
        txtRepeatLiv?.setOnClickListener(repeatLiv)

        btnLiv?.setOnClickListener(livOnClickListener)
        btnLiv?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })


        btnBack = findViewById(R.id.btnBack)
        btnBack?.setOnClickListener(gobackOnClickListener)

        btnCheck = findViewById(R.id.btnCheck)
        btnCheck?.setOnClickListener(checkOnClickListener)

        btnCheck?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        boxHints = findViewById(R.id.boxHints)
        boxResult = findViewById(R.id.boxResult)

        imgLiv = findViewById(R.id.imgLiv)
        blank_result = findViewById(R.id.blank_result)

        btnClose = findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(closeOnClickListener)

        loadingFrame = findViewById(R.id.loadingFrame)

    }

    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    private val livOnClickListener = View.OnClickListener {
        runOnUiThread {
            CallLoading()
            livenessProcess()
        }
    }


    private fun livenessProcess(){

        val intent = Intent(this, LivenessIntro::class.java)
        intent.putExtra("session_id", session_id)
        intent.putExtra("url_wsliv", variables.url_wsliv)
        intent.putExtra("url_lbliv", variables.url_lbliv)
        startActivityForResult(intent, 1)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("Result Code DEMO: $requestCode $resultCode ")
        StopLoading()
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val toc_token = data!!.getStringExtra("liveness_token")
                val b64_image = data?.getStringExtra("image")

                imgSelfie = b64_image

                println("TOC TOKEN: $toc_token")


                liveness_token = toc_token

                val image = Base64.decode(b64_image, Base64.URL_SAFE)
                //byte[] image = data.getByteArrayExtra("image");
                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                    println("************Image on DEMO size: WIDTH= " + b.width + " HEIGHT: " + b.height)

                } catch (e: java.lang.Exception) {
                    b = null
                    e.printStackTrace();
                }

                txtLivhint?.visibility = View.GONE
                btnLiv?.visibility = View.GONE
                boxHints?.visibility = View.GONE


                blank_result?.layoutParams = TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 44f)

                imgLiv?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 404, 720, false))
                imgLiv?.visibility = View.VISIBLE
                imgLiv?.background = getDrawable(R.drawable.rounded_rec)
                imgLiv?.clipToOutline = true

                btnCheck?.visibility = View.VISIBLE
                txtRepeatLiv?.visibility = View.VISIBLE
                boxResult?.visibility = View.VISIBLE

                println("LIVENESS TOKEN: " + toc_token)

                //bringBioResult()

                //i.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                println("RESULT CANCELED ON DEMO")

            }

            if (resultCode == 401) {

            }

            if (resultCode == 405) {

            }
        }

        if(requestCode == 4)
        {
            if(resultCode == 404)
            {
                setResult(404)
                finish()
            }
        }

    }


    private val checkOnClickListener = View.OnClickListener {


        getBioResult()
       //bringBioResult()

    }



    private val repeatLiv = View.OnClickListener {

        val t = Thread(Runnable {
            try {
                val rsp_data = consumeSessionManager()
                val rsp = JSONObject(rsp_data)
                session_id = rsp.getString("session_id")

                runOnUiThread {
                    StopLoading()
                    livenessProcess()
                }
            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()

    }


    private fun bringBioResult(){


        val intent = Intent(this, BioResult::class.java)

        intent.putExtra("id_front", front_token)
        intent.putExtra("id_back", back_token)
        intent.putExtra("selfie", liveness_token)
        intent.putExtra("document_type", document_type)
        intent.putExtra("apiKey", variables.api_key)
        intent.putExtra("imgSelfie", imgSelfie)

        if(noAC == "true")
        {
            intent.putExtra("noAC", "true")

            if(passport == "true")
            {
                intent.putExtra("passport", "true")
            }
        }


        startActivityForResult(intent,4 )
    }

    private fun getBioResult(){

        runOnUiThread {
            CallLoading()
        }

        val t = Thread(Runnable {
            try {
                val rsp_data: String?

                if(noAC == "true")
                {
                    rsp_data = consumeNoACTocApi()
                }else
                {
                    rsp_data = consumeTocApi()
                }

                val rsp = JSONObject(rsp_data)
                println(rsp.toString())

                afterThread(rsp)


            } catch (e: java.lang.Exception) { e.printStackTrace();
            }
        })
        t.start()

    }

    private fun afterThread(rsp:JSONObject) {
        runOnUiThread {
            StopLoading()

            val intent = Intent(this, BioResult::class.java)

            intent.putExtra("id_front", front_token)
            intent.putExtra("id_back", back_token)
            intent.putExtra("selfie", liveness_token)
            intent.putExtra("document_type", document_type)
            intent.putExtra("imgSelfie", imgSelfie)
            intent.putExtra("json", rsp.toString())

            if(noAC == "true")
            {
                intent.putExtra("noAC", "true")
                intent.putExtra("id_front", "ac")
                intent.putExtra("id_back", "ac")

                if(passport == "true")
                {
                    intent.putExtra("passport", "true")
                    intent.putExtra("id_front", "ac")
                    intent.putExtra("id_back", "ac")
                }
            }

            startActivityForResult(intent,4 )

        }
    }


    private fun consumeSessionManager(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("apiKey", variables.api_key)
                .addFormDataPart("mode", "3")
                .addFormDataPart("liveness_passive", "true").build();
            val request: Request = Request.Builder()
                .url(variables.url_session_manager)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }


    private fun consumeTocApi(): String? {
        var resp = ""
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
            val body: RequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("id_front", front_token!!)
                .addFormDataPart("id_back", back_token!!)
                .addFormDataPart("selfie", liveness_token!!)
                .addFormDataPart("documentType", document_type!!)
                .addFormDataPart("apiKey", variables.api_key).build()

            val request: Request = Request.Builder()
                .url(variables.url_face_vs_doc)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }

    private fun consumeNoACTocApi(): String?{
        var resp = ""
        val b1: ByteArray = Base64.decode(front_token, Base64.URL_SAFE)
        val b2: ByteArray = Base64.decode(back_token, Base64.URL_SAFE)
        try {
            val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

            var body: RequestBody? = null
            if(passport != "true")
            {
                body = MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("id_front", "frontal", RequestBody.create(MediaType.parse("image/jpeg"), b1))
                    .addFormDataPart("id_back", "trasera", RequestBody.create(MediaType.parse("image/jpeg"), b2))
                    .addFormDataPart("selfie", liveness_token)
                    .addFormDataPart("documentType", document_type)
                    .addFormDataPart("apiKey", variables.api_key).build()
            }else
            {
                body = MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("id_front", "frontal", RequestBody.create(MediaType.parse("image/jpeg"), b1))
                    .addFormDataPart("selfie", liveness_token)
                    .addFormDataPart("documentType", document_type)
                    .addFormDataPart("apiKey", variables.api_key).build()
            }


            val request: Request = Request.Builder()
                .url(variables.url_face_vs_doc)
                .post(body)
                .addHeader("content-type", "multipart/form-data")
                .addHeader("cache-control", "no-cache")
                .build()
            val response: Response = client.newCall(request).execute()
            resp = response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return resp
    }


    private val infoOnClickListener = View.OnClickListener {
        layout_liv?.visibility = View.GONE
        layout_livinfo?.visibility = View.VISIBLE
    }

    private val gobackOnClickListener = View.OnClickListener {
        layout_livinfo?.visibility = View.GONE
        layout_liv?.visibility = View.VISIBLE
    }

    private fun openPic(side: String) : File {
        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, side + ".jpg")

        return file
    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }

    private fun CallLoading() {
        loadingFrame?.visibility = View.VISIBLE

    }

    private fun StopLoading() {
        loadingFrame?.visibility = View.GONE

    }


}