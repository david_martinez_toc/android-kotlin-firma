package com.example.demo_android

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import org.w3c.dom.Text
import java.io.File

class TocApi : AppCompatActivity()  {

    private var layout_tocApi: View? = null
    private var layout_apiInfo: View? = null
    private var btnBack: ImageView? = null

    private var btnClose: ImageView? = null
    private var btnDoc: Button? = null

    private var front_token: String? = null
    private var back_token: String? = null
    private var liveness_token: String? = null
    private var toc_token: String? = null
    private var document_type: String? = null

    private var txtInfoApi: TextView? = null
    private var txt1st: TextView? = null
    private var txtApihint: TextView? = null
    private var noAC: String? = null
    private var passport: String? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_toc_api)

        front_token = intent.getStringExtra("id_front")
        back_token = intent.getStringExtra("id_back")
        liveness_token = intent.getStringExtra("selfie")
        toc_token = intent.getStringExtra("toc_token")
        document_type = intent.getStringExtra("document_type")

        if(intent.getStringExtra("noAC") != null && intent.getStringExtra("noAC") == "true")
        {
            noAC = intent.getStringExtra("noAC")

            if(intent.getStringExtra("passport") != null && intent.getStringExtra("passport") == "true")
            {
                passport = intent.getStringExtra("passport")
                var fileFront: ByteArray = openPic("front").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)

            }else
            {

                var fileFront: ByteArray = openPic("front").readBytes()
                var fileBack: ByteArray = openPic("back").readBytes()
                front_token = fileFront.toString(Charsets.UTF_8)
                back_token = fileBack.toString(Charsets.UTF_8)

            }

        }


        layout_tocApi = findViewById(R.id.layout_tocapi)
        layout_apiInfo = findViewById(R.id.layout_APInfo)

        txtInfoApi = findViewById(R.id.txtInfoApi)
        txtInfoApi?.setOnClickListener(infoOnClickListener)

        btnBack = findViewById(R.id.btnBack)
        btnBack?.setOnClickListener(gobackOnClickListener)
        btnBack?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnDoc = findViewById(R.id.btnDoc)
        btnDoc?.setOnClickListener(docOnClickListener)
        btnDoc?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnClose = findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(closeOnClickListener)

        txt1st = findViewById(R.id.txt1st)
        formaText(txt1st!!,"La <b><font color='#707070' face='Raleway,Bold'>Firma Electrónica TOC</font></b> es una herramienta que permite incorporar certificados digitales a documentos de tipo PDF a partir de una verificación de identidad positiva. La verificación de identidad TOC (TOC API) actúa como muestra de consentimiento además de validar biométricamente la identidad del firmante.")

        txtApihint = findViewById(R.id.txtApihint)
        formaText(txtApihint!!, "Usa <b><font color='#04D98B'>Firma electrónica</font></b> para<br>firmar documentos")
    }

    private val infoOnClickListener = View.OnClickListener {
        layout_tocApi?.visibility = View.GONE
        layout_apiInfo?.visibility = View.VISIBLE
    }

    private val gobackOnClickListener = View.OnClickListener {
        layout_apiInfo?.visibility = View.GONE
        layout_tocApi?.visibility = View.VISIBLE
    }

    private val docOnClickListener = View.OnClickListener {
        val intent = Intent(this, TocFaceToken::class.java)

        if(noAC == "true")
        {
            intent.putExtra("noAC", "true")
            intent.putExtra("id_front", "ac")
            intent.putExtra("id_back", "ac")

            if(passport == "true")
            {
                intent.putExtra("passport", "true")
            }

        }else
        {
            intent.putExtra("id_front", front_token)
            intent.putExtra("id_back", back_token)
        }

        intent.putExtra("id_front", front_token)
        intent.putExtra("id_back", back_token)
        intent.putExtra("selfie", liveness_token)
        intent.putExtra("documentType", document_type)
        intent.putExtra("apiKey", variables.api_key)
        intent.putExtra("toc_token", toc_token)

        startActivityForResult(intent,6)
    }

    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == 6)
        {
            if(resultCode == 404)
            {
                setResult(404)
                finish()
            }
        }

    }

    private fun openPic(side: String) : File {
        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, side + ".jpg")

        return file
    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }


}