package com.example.demo_android;

public class variables {

    /* SANDBOX

    public static String api_key = "0dd773a7496e48dd81cf3bb56c139e5e";

    public static String url_session_manager = "https://sandbox-api.7oc.cl/session-manager/v1/session-id";
    public static String url_face_vs_doc = "https://sandbox-api.7oc.cl/v2/face-and-document";
    public static String url_face_vs_token = "https://sandbox-api.7oc.cl/v2/face-and-token";
    public static String url_simpleSign = "https://sandbox-api.7oc.cl/signer/v1/simple-sign";
    public static String url_FEA = "https://sandbox-api.7oc.cl/signer/v1/fea";

    public static String url_wsac = "";
    public static String url_lbac = "";
    public static String url_wsliv = "";
    public static String url_lbliv = "";
    */
 /*DEV
    public static String api_key = "0dd773a7496e48dd81cf3bb56c139e5e";

    public static String url_session_manager = "https://dev-api.7oc.cl/session-manager/v1/session-id";
    public static String url_face_vs_doc = "https://dev-api.7oc.cl/v2/face-and-document";
    public static String url_face_vs_token = "https://dev-api.7oc.cl/v2/face-and-token";
    public static String url_simpleSign = "https://dev-api.7oc.cl/signer/v1/simple-sign";
    public static String url_FEA = "https://dev-api.7oc.cl/signer/v1/fea";

    public static String url_wsac = "https://dev-capture.tocws.com";
    public static String url_lbac = "https://dev-api.7oc.cl/auto-capture/v1";
    public static String url_wsliv = "https://dev-liveness.tocws.com";
    public static String url_lbliv = "https://dev-api.7oc.cl/liveness/image-saver/v1";
*/
/* PROD */
    public static String api_key = "ce8d6bb1deb64f8f8b29d3717f9ca02e";

    public static String url_session_manager = "https://prod-api.7oc.cl/session-manager/session-id/v1";
    public static String url_face_vs_doc = "https://prod-api.7oc.cl/facial/face-and-document/v3";
    public static String url_face_vs_token = "https://prod-api.7oc.cl/facial/face-and-token/v3";
    public static String url_simpleSign = "https://prod-api.7oc.cl/signer/simple-sign/v1";
    public static String url_FEA = "https://prod-api.7oc.cl/signer/fea/v1";

    public static String url_wsac = "https://prod-capture.tocws.com";
    public static String url_lbac = "https://prod-api.7oc.cl/auto-capture/data/v2";
    public static String url_wsliv = "https://prod-liveness.tocws.com";
    public static String url_lbliv = "https://prod-api.7oc.cl/liveness/image-saver/v1";




}
