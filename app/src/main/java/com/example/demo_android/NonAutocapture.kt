package com.example.demo_android

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Base64
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.io.File

class NonAutocapture : AppCompatActivity() {

    private var btnfront: Button? = null
    private var btnback: Button? = null
    private var btnClose: ImageView? = null

    private var txtachint: TextView? = null
    private var txtDocType: TextView? = null
    private var txtRepeatFront: TextView? = null
    private var txtRepeatBack: TextView? = null
    private var txtEdit: TextView? = null

    private var btnnext: Button? = null
    private var btnnext2: Button? = null
    private var imgFront: ImageView? = null

    private var front_jpg: String? = null
    private var back_jpg: String? = null

    private var flag: String? = null
    private var document_type: String? = null
    private var imgFlag: ImageView? = null

    private var boxHints: View? = null
    private var boxResult: View? = null
    private var imgDocSide: ImageView? = null

    override fun onBackPressed() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_non_autocapture)


        btnClose = findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(closeOnClickListener)

        txtRepeatFront = findViewById(R.id.txtRepeatFront)
        txtRepeatFront?.setOnClickListener(repeatFront)

        txtRepeatBack = findViewById(R.id.txtRepeatBack)
        txtRepeatBack?.setOnClickListener(repeatBack)

        txtachint = findViewById(R.id.txtachint)
        formaText(txtachint!!,"Toma una foto de la<br>parte <b><font color='#04D98B'>delantera</font></b><br>de tu documento")


        imgFront = findViewById(R.id.imgFront)
        imgDocSide = findViewById(R.id.imgDocSide)

        btnnext = findViewById(R.id.btnnext)
        btnnext?.setOnClickListener(nextOnClickListener)
        btnnext?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnnext2 = findViewById(R.id.btnnext2)
        btnnext2?.setOnClickListener(next2OnClickListener)
        btnnext2?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnfront = findViewById(R.id.btnfront)
        btnfront?.setOnClickListener(frontOnClickListener)
        btnfront?.setOnTouchListener(object:View.OnTouchListener
        {
            override fun onTouch(v:View?, event: MotionEvent?): Boolean {

                if (event?.getAction() == MotionEvent.ACTION_DOWN || event?.getAction() == MotionEvent.ACTION_MOVE)
                {

                    v?.background = resources.getDrawable(R.drawable.rounded_rec_pressed)
                }

                if (event?.getAction() == MotionEvent.ACTION_UP || event?.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v?.background = resources.getDrawable(R.drawable.green_button)
                    //v?.setBackgroundColor(Color.parseColor("#ffffff"))
                }


                return false
            }
        })

        btnback = findViewById(R.id.btnback)
        btnback?.setOnClickListener(backOnClickListener)

        boxHints = findViewById(R.id.boxHints)
        boxResult = findViewById(R.id.boxResult)

        txtEdit = findViewById(R.id.txtEdit)
        txtEdit?.setOnClickListener(editOnClickListener)

        txtDocType = findViewById(R.id.txtDocType)

        imgFlag = findViewById(R.id.imgFlag)
        flag = intent.getStringExtra("doc_flag")
        document_type = intent.getStringExtra("doc_type")

        setDoctype()
        setFlag()


    }

    private val closeOnClickListener = View.OnClickListener {
        setResult(404)
        finish()
    }

    private val frontOnClickListener = View.OnClickListener {
        frontSideCapture()
    }

    private val backOnClickListener = View.OnClickListener {
        backSideCapture()
    }

    private val editOnClickListener = View.OnClickListener {
        setResult(405)
        finish()
    }

    private fun frontSideCapture() {
        val intent = Intent(this, CameraHandler::class.java)
        startActivityForResult(intent, 1)
    }

    private fun backSideCapture() {
        val intent2 = Intent(this, CameraHandler::class.java)
        intent2.putExtra("front_jpg", "yes")
        startActivityForResult(intent2, 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        println("REQUEST CODE: " + requestCode)
        println("RESULT CODE: " + resultCode)
        if (resultCode == 404) {
            setResult(404)
            finish()
        }

        if (requestCode == 1) {

            if (resultCode == 200) {
                //front_jpg = data?.getStringExtra("front_jpg");

                var file = openPic("front")
                var fileByte: ByteArray = file.readBytes()
                val image: ByteArray = Base64.decode(fileByte, Base64.URL_SAFE)

                //byte[] image = data.getByteArrayExtra("image");
                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                } catch (e: Exception) {
                    b = null
                    //e.printStackTrace();
                }

                //txtachint?.visibility = View.GONE
                btnfront?.visibility = View.GONE

                boxHints?.visibility = View.GONE
                boxResult?.visibility = View.VISIBLE

                /*txtRepeatFront?.visibility = View.VISIBLE*/
                btnnext?.visibility = View.VISIBLE
                imgFront?.visibility = View.VISIBLE
                imgFront?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))

            }

        }

        if (requestCode == 2) {
            if (resultCode == 200) {
                println("SEGUNDA FOTO, yay")
                //back_jpg = data?.getStringExtra("back_jpg")

                var file = openPic("back")
                var fileByte: ByteArray = file.readBytes()
                val image: ByteArray = Base64.decode(file.readBytes(), Base64.URL_SAFE)

                var b: Bitmap?
                try {
                    b = BitmapFactory.decodeByteArray(image, 0, image.size)
                } catch (e: Exception) {
                    b = null
                    //e.printStackTrace();
                }

                //txtachint?.visibility = View.GONE
                btnfront?.visibility = View.GONE
                btnback?.visibility = View.GONE
                btnnext?.visibility = View.GONE
                txtRepeatFront?.visibility = View.GONE

                boxHints?.visibility = View.GONE
                boxResult?.visibility = View.VISIBLE

                txtRepeatBack?.visibility = View.VISIBLE
                btnnext2?.visibility = View.VISIBLE
                imgFront?.visibility = View.VISIBLE
                imgFront?.setImageBitmap(Bitmap.createScaledBitmap(b!!, 1024, 720, false))
            }
        }

    }


    private val repeatFront = View.OnClickListener {
        frontSideCapture()
    }

    private val repeatBack = View.OnClickListener {
        backSideCapture()
    }

    private val nextOnClickListener = View.OnClickListener {

        txtachint?.text = "Toma una foto de la parte trasera de tu documento"
        formaText(txtachint!!,"Toma una foto de la\nparte <b><font color='#04D98B'>trasera</font></b>\nde tu documento")

        boxResult?.visibility = View.GONE
        boxHints?.visibility = View.VISIBLE


        txtRepeatFront?.visibility = View.GONE
        imgFront?.visibility = View.GONE
        btnfront?.visibility = View.GONE
        btnnext?.visibility = View.GONE
        btnback?.visibility = View.VISIBLE
        txtachint?.visibility = View.VISIBLE
        imgDocSide?.setImageDrawable(getDrawable(R.drawable.idback))

    }

    private val next2OnClickListener = View.OnClickListener {
        bringPreLiveness()
    }

    private fun bringPreLiveness(){

        val intent = Intent(this, PreLiveness::class.java)
        intent.putExtra("session_id", "")
        intent.putExtra("front_token", "ac")
        intent.putExtra("back_token", "ac")
        intent.putExtra("document_type", document_type)
        intent.putExtra("noAC", "true")
        startActivityForResult(intent, 3)
    }

    private fun setFlag(){

        if(flag == "CHL"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.chl))
        }

        if(flag == "PER")
        {
            imgFlag?.setImageDrawable(getDrawable(R.drawable.per))
        }

        if(flag == "ARG"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.arg))
        }
        if(flag == "MEX"){
            imgFlag?.setImageDrawable(getDrawable(R.drawable.mex))
        }
    }

    private fun setDoctype(){
        if(document_type == "CHL2"){
            formaText(txtDocType!!, "Cédula chilena <b>nueva</b>")
        }

        if(document_type == "CHL1")
        {
            formaText(txtDocType!!, "Cédula chilena <b>antigua</b>")
        }

        if(document_type == "PER1")
        {
            formaText(txtDocType!!, "Cédula peruana <b>antigua</b>")
        }

        if(document_type == "PER2")
        {
            formaText(txtDocType!!, "Cédula peruana <b>nueva</b>")
        }

        if(document_type == "PER3")
        {
            formaText(txtDocType!!, "Documento extranjeria")
        }

        if(document_type == "ARG1")
        {
            formaText(txtDocType!!, "Cédula argentina <b>antigua</b>")
        }

        if(document_type == "ARG2")
        {
            formaText(txtDocType!!, "Cédula argentina <b>nueva</b>")
        }

        if(document_type == "MEX1")
        {
            formaText(txtDocType!!, "ID electoral de México(IFE)")
        }

        if(document_type == "MEX2")
        {
            formaText(txtDocType!!, "ID electoral de México(INE)")
        }
    }

    private fun formaText(tv:TextView, bold:String){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(bold, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(bold));
        }

    }

    private fun openPic(side: String) : File {

        var root = this.filesDir.path
        //var root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root)
        var file = File(myDir, side + ".jpg")

        return file
    }
}



