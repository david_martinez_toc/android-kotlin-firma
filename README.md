# 7OC Demo Android
App demo de los servicios de toc, que incluye Autocaptura, Liveness, FaceVsDocument, FaceVsToken y Firma, para ser utilizado como ejemplo de implementacion por los clientes

###### NOTA: Esta demo tiene las credenciales de sandbox de toc y tambien prod de toc.
Imagen de la app:
![imagen](demo_image.jpg)